jQuery(document).ready(function($) {

	// Wow JS
	var scrolled = false;
	wow = new WOW(
	{
	   boxClass:     'wow',      // default
	   animateClass: 'animated', // default
	   offset:       0,          // default
	   mobile:       false,       // default
	   live:         true        // default
	});
	$(window).on('scroll', function() {
		if (!scrolled) {
		scrolled = true;
		wow.init();
		}
	});

});