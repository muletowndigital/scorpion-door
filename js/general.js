jQuery(document).ready(function($) {

	$(window).scroll();

	// Header Clone

	//$('.masthead .container').clone().prependTo('.masthead--fixed');

	// Mobile Menu Trigger

	$('.js-mm-trigger').on('click', function(e) {
		$(this).toggleClass('js-mm-close');
		$('.mm').toggleClass('mm--show');
		$('body').toggleClass('locked');
		e.preventDefault();
	});

	/***********************************************************************************/
	/* Local Scrolling */
	/***********************************************************************************/

	$('a[href*="#"]')
	  // Remove links that don't actually link to anything
	  .not('[href="#"]')
	  .not('[href="#0"]')
	  .click(function(event) {
	    // On-page links
	    if (
	      location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
	      // Figure out element to scroll to
	      var target = $(this.hash);
	      var target_offset = 0;

	      if($(window).width() > 1040) {
	      	target_offset = 0;
	      }

	      else {
	      	target_offset = $('.masthead').outerHeight();
	      }

	      target = target.length ? target : $('.' + this.hash.slice(1));
	      // Does a scroll target exist?
	      if (target.length) {
	        // Only prevent default if animation is actually gonna happen
	        event.preventDefault();
	        $('html, body').animate({
	          scrollTop: target.offset().top - target_offset
	        }, 1000, function() {
	          // Callback after animation
	          // Must change focus!
	          var $target = $(target);

	          if ($target.is(":focus")) { // Checking if the target was focused
	            return false;
	          } else {
	            //$target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
	           // $target.focus(); // Set focus again
	          }
	        });
	      }
	   }
	});

	/***********************************************************************************/
	/* Quote Slider */
	/***********************************************************************************/

	if($('.testimonials__slider').length) {

		$('.testimonials__slider').slick({
			dots: false,
			arrows: true,
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: false,
	  		autoplaySpeed: 20000,
			slide: 'article',
			prevArrow: '.slide-arrow--prev',
			nextArrow: '.slide-arrow--next',
			responsive: [
			{
				breakpoint: 815,
				settings: {
				  arrows: false,
				  dots: true,
				  adaptiveHeight: true,
				}
			},
			]

		});
	}

	/***********************************************************************************/
	/* Quote Slider */
	/***********************************************************************************/

	/*if($('.quotes__slider').length) {

		$('.quotes__slider').slick({
			dots: true,
			appendDots: $('.dots-holder'),
			arrows: false,
			infinite: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: false,
	  		autoplaySpeed: 8000,
			slide: 'article',
			responsive: [
			{
				breakpoint: 815,
				settings: {
				  arrows: false,
				  dots: true,
				}
			},
			]

		});
	}*/

	/***********************************************************************************/
	/* Resize Functions */
	/***********************************************************************************/

	// Window Resize Functions

	$(window).resize(function(){

		var window_width = $(window).width();

		if(window_width > 835) {
			$('.js-mm-trigger').removeClass('js-mm-close');
			$('.mm').removeClass('mm--show');
			$('body').removeClass('locked');
		}

	});

	$(window).resize();

	/***********************************************************************************/
	/* Scrolling Functions */
	/***********************************************************************************/

	var lastScrollTop = 0;

	$(window).scroll(function () {
		var top_pos = $(window).scrollTop();
		//var window_width = $(window).width();


		if (top_pos >= 100) {
			$(".nav-triggers").addClass("hamburger-bg");
		} else {
			$(".nav-triggers").removeClass("hamburger-bg");
		}

		if (top_pos < lastScrollTop && top_pos > 200){
	      // upscroll code
	   	$('.masthead--fixed').addClass('masthead--fixed--on');
	   	$('body').addClass('header-lock');
	   }

	   else {
	      // upscroll code
	   	$('body').removeClass('header-lock');
	   	$('.masthead--fixed').removeClass('masthead--fixed--on');
	   }

		if(top_pos > 100) {
			$('.nav-triggers a').addClass('mm--scrolled');
		}

		else {
			$('.nav-triggers a').removeClass('mm--scrolled');
		}

  		lastScrollTop = top_pos;

	});

	$(window).scroll();

});

/*$( window ).on( 'elementor/frontend/init', function() {
    //hook name is 'frontend/element_ready/{widget-name}.{skin} - i dont know how skins work yet, so for now presume it will
    //always be 'default', so for example 'frontend/element_ready/slick-slider.default'
    //$scope is a jquery wrapped parent element
    elementorFrontend.hooks.addAction( 'frontend/element_ready/shortcode.default', function($scope, $){
    	if($('.quotes__slider').length) {
        $scope.find('.quotes__slider').slick({
            dots: true,
				appendDots: $('.dots-holder'),
				arrows: false,
				infinite: false,
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: false,
		  		autoplaySpeed: 8000,
				slide: 'article',
				responsive: [
				{
					breakpoint: 815,
					settings: {
					  arrows: false,
					  dots: true,
					}
				},
				]
        });
      }
    });
});*/
