<?php
$settings = get_option('theme_options');
$footer_logo = get_field('footer_logo', 'options');
$company_address = get_field('company_address', 'options');
$company_email_address = get_field('company_email_address', 'options');
$company_phone_number = get_field('company_phone_number', 'options');
$copyright_statement = get_field('copyright_statement', 'options');
$cta = get_field('footer_cta_text', 'options');
$cta_btn = get_field('footer_cta_button', 'options');
?>

<!-- Footer -->

<footer class="footer">

    <div class="container">
        <div class="row footer-row">
            <?php if ($footer_logo) { ?>
                <div class="footer__logo">
                    <a href="/">
                        <?php echo wp_get_attachment_image($footer_logo['ID'], 'medium', false, array('class' => '')); ?>
                    </a>
                </div>
            <?php } ?>

            <?php if ($company_address || $company_phone_number || $company_email_address) { ?>
                <div class="footer__contacts">
                    <?php if ($company_address) { ?>
                        <div class="footer__address">
                            <?php echo $company_address; ?>
                        </div>
                    <?php } ?>
                    <?php if ($company_phone_number) { ?>
                        <a class="footer__phone"
                           href="tel:<?php echo $company_phone_number; ?>"><?php echo $company_phone_number; ?></a>
                    <?php } ?>
                    <?php if ($company_email_address) { ?>
                        <a class="footer__mail"
                           href="mailto:<?php echo $company_email_address; ?>"><?php echo $company_email_address; ?></a>
                    <?php } ?>
                </div>
            <?php } ?>

            <?php if($cta || $cta_btn) { ?>
                <div class="footer__cta">
                    <?php if($cta) { ?>
                    <h4 class="footer__cta-heading"><?php echo $cta ;?></h4>
                    <?php } ?>
                    <?php if ($cta_btn):
                        $link_url = $cta_btn['url'];
                        $link_title = $cta_btn['title'];
                        $link_target = $cta_btn['target'] ? $cta_btn['target'] : '_self'; ?>
                        <a class="btn btn--two btn--norm footer__cta-btn"
                           href="<?php echo esc_url($link_url); ?>"
                           target="<?php echo esc_attr($link_target); ?>"><i class="fas fa-phone"></i> <?php echo esc_html($link_title); ?></a>
                    <?php endif; ?>
                </div>
            <?php } ?>

        </div>

    </div>
    <?php if ($copyright_statement) { ?>
        <div class="row footnotes">
            <div class="footer__copyright">
                <?php echo $copyright_statement; ?>
            </div>
        </div>
    <?php } ?>

</footer>

</div>

<section class="mm mobile-menu-container">

    <nav class="mm__menu mobile-menu">
        <?php
        wp_nav_menu(array(
            'theme_location' => 'mobile-menu'
        ));
        ?>
    </nav>

    <div class="mm__contacts">
        <?php if ($company_address) { ?>
            <div class="mm__address">
                <?php echo $company_address; ?>
            </div>
        <?php } ?>
        <?php if ($company_phone_number) { ?>
            <a class="mm__phone"
               href="tel:<?php echo $company_phone_number; ?>"><?php echo $company_phone_number; ?></a>
        <?php } ?>
        <?php if ($company_email_address) { ?>
            <a class="mm__mail"
               href="mailto:<?php echo $company_email_address; ?>"><?php echo $company_email_address; ?></a>
        <?php } ?>
    </div>

</section>

<?php wp_footer(); ?>

</body>
</html>
