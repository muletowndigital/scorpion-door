<?php

	// Search Results Page

	get_header();	
?>

<!-- Start Main Content -->

<main class="internal-page">

	<section class="search_results has_sidebar" id="post-<?php the_ID(); ?>">

		<!-- Container -->
		
		<div class="container">

			<h1 class="pg_title">Search Results</h1>

			<h4 class="search_data">Searching for <i>"<?= $_GET['s']; ?>"</i></h4>
			
			<!-- WP Content -->
									
			<article class="wp-content span_8" id="search-results">

				<div class="row">

					<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

					<?php 

						$link = get_the_permalink(); 

						if(!preg_match('/style-guide/', $link)) {
							get_template_part('posts', get_post_format());
						}

					?>
												
					<?php endwhile; ?>

				</div>
				
				<?php wp_pagenavi(); ?>
				
				<?php else: ?>
				
				<p>We are sorry but there were no posts matching your search criteria.</p>
				
				<div class="wp_search_form">
					<?php get_search_form(); ?>
				</div>
				
				<?php endif; ?>
					
			</article>
			
			<!-- If Right Sidebar -->
			
			<aside class="blog-sidebar span_4 omega">
				
			<?php if (is_active_sidebar('blog-sidebar')) : ?>
				<?php dynamic_sidebar('blog-sidebar'); ?>
			<?php endif;?>
				
			</aside>
					
		</div>
			
	</section>

</main>

<?php

	get_footer();
	
?>