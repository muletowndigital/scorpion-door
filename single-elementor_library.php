<?php
	
	// Elementor - SINGLE TEMPLATE

	get_header();

	$blog_page = get_option( 'page_for_posts' );

	$post_id = get_the_ID();
	
?>

<main class="internal-page wp-content single-ele-template">

	<div class="page-container wp-content">

		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

			<?php the_content(); ?>
	
		<?php endwhile; endif; ?>		

	</div>

</main>

<?php get_footer(); ?>