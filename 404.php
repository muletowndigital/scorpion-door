<?php

	/* 404 Error Page */

	get_header();

	// The Page ID
	$page_id = get_the_ID();
?>

<main class="wp-content error-page">

	<?php //include('include/page-hero.php'); ?>

	<!-- Begin Editor Page Content -->

	<section class="page-content internal-page error-page">
		
		<div class="container">

			<div style="text-align: center; margin: auto;">

				<h1 class="text--primary">404 Error Page</h1>
							
				<h6 class="text--primary">Oops. The page you are looking for is no longer available.</h6>

				<div class="button-404" style="margin-top: 40px;">
					<a href="/" class="btn btn--primary">Go Back to Home</a>
				</div>

			</div>
					
		</div>
			
	</section>

</main>

<?php

	get_footer();
	
?>