<?php

// Global Variables

$site_name = get_bloginfo('name');
$site_author = "Muletown Digital";
$settings = get_option('theme_options');
$phone = get_field('company_phone_number', 'options');


?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

    <!-- Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="author" content="<?= $site_author; ?>">

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicons/ms-icon-144x144.png">

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?> id="post-<?php echo get_the_ID(); ?>">

<div class="full-wrapper">

    <!-- Header Main -->

    <header class="masthead">

        <div class="container">

            <div class="masthead__flex">

                <div class="masthead__logo">
                    <a href="/">
                        <?php
                        $header_logo = get_field('company_logo_color', 'option');
                        $m_logo = get_field('mobile_logo', 'options');

                        if (!empty($header_logo)) {
                            echo '<img class="desktop-logo" src="' . $header_logo['url'] . '" alt="' . $header_logo['alt'] . '" />';
                        } else {
                            echo $site_name;
                        }

                        if (!empty($m_logo)) {
                            echo '<img class="mobile-logo" src="' . $m_logo['url'] . '" alt="' . $m_logo['alt'] . '" />';
                        } else {
                            echo $site_name;
                        }
                        ?>
                    </a>
                </div>


                <nav class="masthead__nav main-navigation">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'main-menu'
                    ));
                    ?>
                </nav>

                <!-- Mobile Triggers -->

                <div class="mobile-nav-wrap">

                    <?php if ($phone): ?>
                        <a class="phone-mobile"
                           href="tel:<?php echo $phone; ?>"
                           target="_self"><i class="fas fa-phone"></i></a>
                    <?php endif; ?>

                    <div class="nav-triggers">
                        <a class="hamburger js-mm-trigger" type="button" role="button" aria-label="Toggle Navigation">
                            <span class="line1"></span>
                            <span class="line2"></span>
                            <span class="line3"></span>
                        </a>
                    </div>

                </div>

            </div>

        </div>

    </header>


    <header class="masthead masthead--fixed">
        <div class="masthead__flex">

            <div class="masthead__logo">
                <a href="/">
                    <?php
                    $sticky_header_logo = get_field('sticky_header_logo', 'option');
                    $m_logo = get_field('mobile_logo', 'options');

                    if (!empty($sticky_header_logo)) {
                        echo '<img class="desktop-logo" src="' . $sticky_header_logo['url'] . '" alt="' . $sticky_header_logo['alt'] . '" />';
                    } else {
                        echo $site_name;
                    }

                    if (!empty($m_logo)) {
                        echo '<img class="mobile-logo" src="' . $m_logo['url'] . '" alt="' . $m_logo['alt'] . '" />';
                    } else {
                        echo $site_name;
                    }
                    ?>
                </a>
            </div>


            <nav class="masthead__nav main-navigation">
                <?php
                wp_nav_menu(array(
                    'theme_location' => 'main-menu'
                ));
                ?>
            </nav>

            <!-- Mobile Triggers -->

            <div class="mobile-nav-wrap">

                <?php if ($phone): ?>
                    <a class="phone-mobile"
                       href="tel:<?php echo $phone; ?>"
                       target="_self"><i class="fas fa-phone"></i></a>
                <?php endif; ?>

                <div class="nav-triggers">
                    <a class="hamburger js-mm-trigger" type="button" role="button" aria-label="Toggle Navigation">
                        <span class="line1"></span>
                        <span class="line2"></span>
                        <span class="line3"></span>
                    </a>
                </div>

            </div>

        </div>
    </header>
