<?php
	
	// Author Archive
	
	get_header();

	$page_id = get_option( 'page_for_posts' );

	// Sidebar Selection
	$sidebar_selection = get_post_meta( $page_id, 'sidebar_selection', true);

	if(empty($sidebar_selection) || $sidebar_selection == "") {
		$sidebar_selection = "off";
		$content_size = "full-page";
		$body_class = "no-sidebar";
	}
	else {
		$content_size = "span_8";
		$body_class = "has-sidebar";
	}

	// Page Title
	$title_display = get_post_meta( $page_id, 'title_display', true);

	// Title Text
	$title_text = get_post_meta( $page_id, 'title_text', true);
	
	// LayerSlider
	$revolution_slider = get_post_meta( $page_id, 'silverbox_slider', true);

	// Featured IMage
	$featured_hero = wp_get_attachment_image_src( get_post_thumbnail_id($page_id), 'full' );
		
?>

<?php if($revolution_slider != "" || $featured_hero != "") { ?>

<!-- Start Hero Section -->

<section class="feature blog-feature">
					
	<!-- Feature Slider -->

	<?php if($revolution_slider != "" && (isset($revolution_slider))) { ?>
	
	<div class="hero" id="heroSlider">
	
		<?php layerslider($revolution_slider); ?>
		
	</div>
	
	<?php } else if($featured_hero != "") { 

		$thumb = $featured_hero;
		$url = $thumb['0'];
		$height = $thumb['2'];

	?>

	<!-- Hero Background Image Added To Page -->

	<div class="hero" id="heroImage" style="background-image: url(<?= $url; ?>); max-height: <?= $height . 'px'; ?>; height: <?= $height . 'px'; ?>"></div>

	<?php } ?>

</section>

<?php } ?>

<!-- Blog / Page Content -->

<section class="page-content internal-page blog-wrapper">
	
	<div class="container">

		<h3 class="archive-title"><?php printf( __( 'Author Archives: %s', 'silverbox' ), '<span class="vcard">' . get_the_author() . '</span>' ); ?></h3>
		
		<div class="flex-container <?= $body_class; ?>">
		
			<!-- Article -->
							
			<div class="wp-content <?= $content_size; ?>" id="wp_content_<?php the_ID(); ?>">
	
				<div class="row">
		
					<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	
					<?php get_template_part('posts', get_post_format()); ?>
				
					<?php endwhile; endif; ?>
	
				</div> <!-- /row -->
				
				<?php wp_pagenavi(); ?>
					
			</div>
			
			<!-- If Right Sidebar -->
			<?php if($sidebar_selection != "off" && isset($sidebar_selection)) { ?>
				<aside class="sidebar blog-sidebar span_4 last">
					<?php if (is_active_sidebar($sidebar_selection)) : ?>
						<?php dynamic_sidebar($sidebar_selection); ?>
					<?php endif;?>
				</aside>
			<?php } ?>
			
		</div> <!-- /flex-container-->

	</div>
					
</section>


<?php

	get_footer();
	
?>