<?php
/***********************************************************************************/
/* Template For Standard Post Loop Parts */
/***********************************************************************************/

$cat_name = ''; // I have this set in some shortcodes

if (!isset($cat_name) || $cat_name == '') {

  if ( class_exists('WPSEO_Primary_Term') ) {

    // Show the post's 'Primary' category, if this Yoast feature is available, & one is set. category can be replaced with custom terms

    $wpseo_primary_term = new WPSEO_Primary_Term( 'category', get_the_id() );

    $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
    $term               = get_term( $wpseo_primary_term );

    if (is_wp_error($term)) {
        $categories = get_the_terms(get_the_ID(), 'category');
        $cat_name = $categories[0]->name;
        $cat_link = get_category_link($categories[0]->term_id);
    } else {
        $cat_name = $term->name;
        $cat_link = get_category_link($term->term_id);
    }

  } else {
    $categories = get_the_category(get_the_ID(), 'category');
    $cat_name = $categories[0]->name;
    $cat_link = get_category_link($categories[0]->term_id);
  }
}

?>

<article class="blog-post">

	<div class="blog-post__image">
		<a href="<?php the_permalink(); ?>">
		<?php
			if(has_post_thumbnail()) {	
				the_post_thumbnail('post-thumb');
			}
			else {
				echo '<img src="' . IMAGES . '/blog-placeholder.jpg" alt="' . get_bloginfo('name') . ' Blog" />';
			}
		?>
		</a>
	</div>
	
	<?php if($cat_name != '') { ?>

	<div class="blog-post__cat">
		<h6><a href="<?= $cat_link; ?>"><?= $cat_name; ?></a></h6>
	</div>

	<?php } ?>
						
	<div class="blog-post__content">
		<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
	</div>
	
</article>