(function() {
   tinymce.create('tinymce.plugins.silverbox', {
      init : function(ed, url) {

         // Custom Buttons

         ed.addButton('custom_buttons', {
            title : 'Custom Buttons',
            image : url+'/images/icon-button.jpg',
            onclick : function() {
               ed.windowManager.open({
                    title: 'Add a New Button',
                    body: [
                        {
                           type: 'textbox',
                           name: 'button_text',
                           label: 'Button Text',
                        },
                        {
                           type   : 'listbox',
                           name   : 'button_class',
                           label  : 'Button Style',
                           values : [
                              { text: 'Primary', value: 'btn--standard btn--primary' },
                              { text: 'Secondary', value: 'btn--standard btn--secondary' },
                              { text: 'Tertiary', value: 'btn--standard btn--tertiary' },
                              { text: 'White', value: 'btn--standard btn--white' },
                           ]
                        },
                        {
                           type: 'textbox',
                           name: 'button_link',
                           label: 'Button Link',
                        },
                        {
                           type: 'textbox',
                           name: 'button_page_id',
                           label: 'Button Page ID (Instead of Link)',
                        },
                        {
                           type   : 'listbox',
                           name   : 'button_align',
                           label  : 'Button Alignment',
                           values : [
                              { text: 'None', value: 'none' },
                              { text: 'Left', value: 'left' },
                              { text: 'Right', value: 'right' },
                              { text: 'Center', value: 'center' },
                           ]
                         },
                         {
                           type   : 'listbox',
                           name   : 'button_link_target',
                           label  : 'Button Link Target',
                           values : [
                              { text: 'None', value: '' },
                              { text: 'New Window', value: '_blank' },
                           ]
                         },
                    ],
                    onsubmit: function(e) {    
                        var button_text = e.data.button_text;
                        var button_class = e.data.button_class;
                        var button_link = e.data.button_link;
                        var button_page_id = e.data.button_page_id;
                        var button_align = e.data.button_align;
                        var button_link_target = e.data.button_link_target;

                        ed.selection.setContent('[button title="' + button_text + '" class="' + button_class + '" link="' + button_link + '" align="' + button_align + '" target="' + button_link_target + '" page_id="' + button_page_id + '"]');
                    }
                });
            }
         });

         // Text Buttons

          ed.addButton('text_buttons', {
            title : 'Text Buttons',
            image : url+'/images/icon-button.jpg',
            onclick : function() {
               ed.windowManager.open({
                    title: 'Add a New Text Button',
                    body: [
                        {
                           type: 'textbox',
                           name: 'button_text',
                           label: 'Button Text',
                        },
                        {
                           type   : 'listbox',
                           name   : 'button_class',
                           label  : 'Button Style',
                           values : [
                              { text: 'Primary', value: 'btn-text btn-text--primary' },
                              { text: 'Secondary', value: 'btn-text btn-text--secondary' },
                              { text: 'Tertiary', value: 'btn-text btn-text--tertiary' },
                              { text: 'White', value: 'btn-text btn-text--white' },
                           ]
                        },
                        {
                           type: 'textbox',
                           name: 'button_link',
                           label: 'Button Link',
                        },
                        {
                           type: 'textbox',
                           name: 'button_page_id',
                           label: 'Button Page ID (Instead of Link)',
                        },
                        {
                           type   : 'listbox',
                           name   : 'button_align',
                           label  : 'Button Alignment',
                           values : [
                              { text: 'None', value: 'none' },
                              { text: 'Left', value: 'left' },
                              { text: 'Right', value: 'right' },
                              { text: 'Center', value: 'center' },
                           ]
                         },
                         {
                           type   : 'listbox',
                           name   : 'button_link_target',
                           label  : 'Button Link Target',
                           values : [
                              { text: 'None', value: '' },
                              { text: 'New Window', value: '_blank' },
                           ]
                         },
                    ],
                    onsubmit: function(e) {    
                        var button_text = e.data.button_text;
                        var button_class = e.data.button_class;
                        var button_link = e.data.button_link;
                        var button_page_id = e.data.button_page_id;
                        var button_align = e.data.button_align;
                        var button_link_target = e.data.button_link_target;

                        ed.selection.setContent('[text_button title="' + button_text + '" class="' + button_class + '" link="' + button_link + '" align="' + button_align + '" target="' + button_link_target + '" page_id="' + button_page_id + '"]');
                    }
                });
            }
         });

         // Font Awesome Icons

         ed.addButton('fontawesome', {
            title : 'Font Awesome Icon',
            image : url+'/images/icon-font_awesome.jpg',
            onclick : function() {
               ed.windowManager.open({
                    title: 'Font Awesome Options',
                    body: [
                        {
                           type: 'textbox',
                           name: 'icon_id',
                           label: 'Icon ID (ex: fa-youtube)',
                        },
                        {
                           type: 'textbox',
                           name: 'icon_size',
                           label: 'Icon Size (ex: 36)',
                        },
                        {
                          type: 'textbox',
                          name: 'icon_link',
                          label: 'Link for Icon (ex: http://google.com)',
                        },
                        {
                           type   : 'listbox',
                           name   : 'icon_link_target',
                           label  : 'Icon Link Target',
                           values : [
                              { text: 'None', value: '' },
                              { text: 'Same Window', value: '_self' },
                              { text: 'New Window', value: '_blank' },
                           ]
                         },
                    ],
                    onsubmit: function(e) {    
                        var icon_id = e.data.icon_id;
                        var icon_size = e.data.icon_size;
                        var icon_link = e.data.icon_link;
                        var icon_link_target = e.data.icon_link_target;

                        if(icon_id === "" || icon_id === null) {
                           return false;
                        }

                        else {

                           if(title_text === "" || title_text === null) {
                              title_text = "";
                           }

                           ed.selection.setContent('[fa icon="' + icon_id + '" size="' + icon_size + '" link="' + icon_link + '" target="' + icon_link_target + '"]');
                        }
                    }
                });
            }
         });

         // Responsive Video Insert

         ed.addButton('customvideo', {
            title : 'Responsive Video',
            image : url+'/images/icon-video.jpg',
            onclick : function() {
               ed.windowManager.open({
                    title: 'Video Embed Options',
                    body: [
                        {
                           type   : 'listbox',
                           name   : 'video_type',
                           label  : 'Choose Video Location',
                           values : [
                              { text: 'Youtube', value: 'youtube' },
                              { text: 'Vimeo', value: 'vimeo' },
                           ]
                         },
                         {
                           type: 'textbox',
                           name: 'video_id',
                           label: 'Video ID (ex: QILiHiTD3uc)',
                        },
                    ],
                    onsubmit: function(e) {    
                        var video_type = e.data.video_type;
                        var video_id = e.data.video_id;

                        if(video_type === "" || video_type === null || video_id == "" || video_id === null) {
                           return false;
                        }

                        if(video_type === "youtube") {
                           ed.selection.setContent('[youtube id="' + video_id + '"]');
                        }

                        else if(video_type === "vimeo") {
                           ed.selection.setContent('[vimeo id="' + video_id + '"]');
                        }

                        else {
                           return false;
                        }
                    }
                });
            }
         });

      },
      createControl : function(n, cm) {
         return null;
      },
      getInfo : function() {
         return {
            longname : "Silverbox MCE Buttons",
            author : 'Adam Silverman',
            authorurl : 'https://silverboxdev.com',
            infourl : '',
            version : "1.0"
         };
      }
   });
   tinymce.PluginManager.add('silverbox', tinymce.plugins.silverbox);


})();