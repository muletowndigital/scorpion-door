<?php

// Testimonials Slider

function testimonials_slider($atts, $content = null) {
   extract(shortcode_atts(array('limit' => '-1'), $atts));
   $page_id = get_the_ID();

   if(get_post_meta($page_id, 'quotes_to_show') != '') {
   	$quotes_to_show = get_post_meta($page_id, 'quotes_to_show');
   	$quotes_array = array();
   	foreach($quotes_to_show as $qts) {
   		array_push($quotes_array, $qts);
   	}

   	$args = array(
			'post_type' => 'quotes',
			'posts_per_page' => $limit,
			'orderby' => 'menu_order',
			'post__in' => $quotes_array,
		);
   }

   else {
   	$args = array(
			'post_type' => 'quotes',
			'posts_per_page' => $limit,
			'orderby' => 'menu_order',
		);
   }

	$loop = new WP_Query($args);
	if($loop->have_posts() ) :
		$quote_display = '';
		$quote_display .= '<div class="quotes">';
			$quote_display .= '<div class="container">';
				$quote_display .= '<div class="quotes__slider">';
					while ( $loop->have_posts() ) : $loop->the_post();
						$quote_id = get_the_ID();

						// Check for Featured Image
						$featured_image = get_the_post_thumbnail($quote_id, 'thumbnail');

						if($featured_image != '') {
							$quote_class = "quote-featured-image";
						}

						else {
							$quote_class = "quote-no-featured-image";
							$featured_image = '<i class="fal fa-user"></i>';
						}

						$quote_display .= '<article class="quote ' . $quote_class . '">';

							if($featured_image != '') {
								$quote_display .= '<figure class="quote__image">' . $featured_image . '</figure>';
							}

							$quote_display .= '<div class="quote__content-wrap">';
								$quote_display .= '<div class="quote__content">' . wpautop(get_the_content($quote_id)) . '</div>';
								$quote_display .= '<div class="quote__author">';

								if(get_post_meta( $quote_id, 'quote_author_name', true) == '') {
									$person_name = get_the_title($quote_id);
								}
								else {
									$person_name = get_post_meta( $quote_id, 'quote_author_name', true);
								}

								$quote_display .= '<h6>' . $person_name . '</h6>';

								if(get_post_meta( $quote_id, 'quote_author_data', true) != '') {
									$quote_display .= '<div class="quote__author-data"><p>' . get_post_meta( $quote_id, 'quote_author_data', true) . '</p></div>';
								}
							$quote_display .= '</div>';
							$quote_display .= '</div>';
						$quote_display .= '</article>';
					endwhile;

				$quote_display .= '</div>';
				$quote_display .= '<div class="dots-holder"></div>';
			$quote_display .= '</div>';
		$quote_display .= '</div>';

	endif; wp_reset_postdata();
	return $quote_display;
}

add_shortcode('testimonials_slider', 'testimonials_slider');

?>