<?php

// Add Muletown Digital Elementor Functions

function add_elementor_widget_categories( $elements_manager ) {

	$elements_manager->add_category(
		'muletown-digital-elementor',
		[
			'title' => __( 'Muletown Digital Custom', 'plugin-name' ),
			'icon' => 'fa fa-plug',
		]
	);
	/*$elements_manager->add_category(
		'second-category',
		[
			'title' => __( 'Second Category', 'plugin-name' ),
			'icon' => 'fa fa-plug',
		]
	);*/

}
add_action( 'elementor/elements/categories_registered', 'add_elementor_widget_categories' );

class My_Elementor_Widgets {

	protected static $instance = null;

	public static function get_instance() {
		if ( ! isset( static::$instance ) ) {
			static::$instance = new static;
		}

		return static::$instance;
	}

	protected function __construct() {
		//require_once('widget-test.php');
		require_once('mtd-button-widget.php');
		require_once('mtd-text-button-widget.php');
		require_once('mtd-arrow-button-widget.php');
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'register_widgets' ] );
	}

	public function register_widgets() {
		//\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\My_Widget_1() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\MTD_Custom_Button() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\MTD_Text_Button() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\MTD_Arrow_Button() );
	}

}

add_action( 'init', 'my_elementor_init' );
function my_elementor_init() {
	My_Elementor_Widgets::get_instance();
}