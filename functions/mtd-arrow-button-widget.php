<?php
namespace Elementor;

class MTD_Arrow_Button extends Widget_Base {
	
	public function get_name() {
		return 'mtd-arrow-button-widget';
	}
	
	public function get_title() {
		return 'Arrow Button';
	}
	
	public function get_icon() {
		return 'fad fa-arrow-right';
	}
	
	public function get_categories() {
		return [ 'muletown-digital-elementor' ];
	}
	
	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Content', 'elementor' ),
			]
		);
		
		$this->add_control(
			'button_text',
			[
				'label' => __( 'Button Text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your button text', 'elementor' ),
			]
		);

		$this->add_control(
			'button_style',
			[
				'label' => __( 'Button Style', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'arrow-btn--one',
				'options' => [
					'arrow-btn--one'  => __( 'Button One', 'plugin-domain' ),
					'arrow-btn--two'  => __( 'Button Two', 'plugin-domain' ),
					'arrow-btn--three'  => __( 'Button Three', 'plugin-domain' ),
				],
			]
		);

		$this->add_control(
			'button_align',
			[
				'label' => __( 'Alignment', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'plugin-domain' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'plugin-domain' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'plugin-domain' ),
						'icon' => 'fa fa-align-right',
					],
				],
				'default' => 'left',
				'toggle' => true,
			]
		);

		$this->add_control(
			'button_link',
			[
				'label' => __( 'Link', 'elementor' ),
				'type' => Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'elementor' ),
				'default' => [
					'url' => '',
					'is_external' => false,
					'nofollow' => false,
				]
			]
		);

		$this->end_controls_section();
	}
	
	protected function render() {

      $settings = $this->get_settings_for_display();

      // Button Settings
		$target = $settings['button_link']['is_external'] ? ' target="_blank"' : '';
		$nofollow = $settings['button_link']['nofollow'] ? ' rel="nofollow"' : '';
      $url = $settings['button_link']['url'];
		echo  '<div class="button-container button-container--arrow button-container--' . $settings['button_align'] . '"><a href="' . $settings['button_link']['url'] . '" class="arrow-btn ' . $settings['button_style'] . '"' . $target . $nofollow .'>' . $settings['button_text'] . '</a></div>';
		 

	}
	
	protected function _content_template() {

   }
	
	
}
?>