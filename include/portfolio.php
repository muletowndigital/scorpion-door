<!-- Portfolio Area -->

<section class="portfolio">
	
	<div class="wide-container">

		<div class="portfolio__flex">
	
			<?php

				// Grab Repeated Group Boxes

				$featured_projects = rwmb_meta('portfolio_group', '', 7);

				// Loop

				foreach($featured_projects as $featured_project) {

					// Grab Image
					$project_image = $featured_project['portfolio_image'][0];
					$project_image_src = wp_get_attachment_image_src($project_image, 'full');
					$project_image_alt = get_post_meta($project_image , '_wp_attachment_image_alt', true);
					$project_page_id = $featured_project['work_page_id'];

					if($project_page_id == '') {
						$project_link = '#';
					}

					else {
						$project_link = get_page_link($project_page_id);
					}

			?>

			<article class="f-project">

				<a href="<?= $project_link; ?>">

					<figure class="f-project__image">
						<img src="<?= $project_image_src[0]; ?>" alt="<?= $project_image_alt; ?>" />
					</figure>

					<div class="f-project__title">

						<div class="f-project__title-ele" data-href="<?= $project_link; ?>">
							<h5><?= $featured_project['portfolio_title']; ?></h5>
							<p><i><?= $featured_project['portfolio_cat']; ?></i></p>
						</div>

					</div>

				</a>

			</article>
		
			<?php } ?>

		</div>

	</div>

</section>