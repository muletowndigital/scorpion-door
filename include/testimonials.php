<!-- testimonials -->

<div class="testimonials">

	<div class="testimonials__container">

		<figure class="quote-element">
			<img src="<?= IMAGES; ?>/quote-element.svg" alt="Quote" />
		</figure>

		<div class="testimonials__slider">

			<a href="#" class="slide-arrow slide-arrow--prev">
				<i class="fal fa-chevron-left"></i>
			</a>

			<?php
				
				$args = array(
					'post_type' => 'quotes',
					'posts_per_page' => -1,
					'orderby' => 'rand',
				);
			
				$loop = new WP_Query($args);
			
				if($loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();
			
			?>
			
			<article class="quote">

				<div class="quote__inside">
				
					<div class="quote__content">
						<?= the_content(); ?>
					</div>

					<div class="quote__author">

						<p>
							<?php

								if(get_field( 'quote_data' ) != '') {
									$author_data_string = ', ' . get_field('quote_data');
								}

								else {
									$author_data_string = '';
								}

								if(get_field('quote_author') == '') {
									echo '<strong>' . get_the_title() . '</strong>' . $author_data_string;
								}

								else {
									echo '<strong>' . get_field('quote_author') . '</strong>' . $author_data_string;
								}
							?>

						</p>
					</div>

				</div>

			</article>

			<?php
				// Code Goes Here
				endwhile; endif; wp_reset_postdata();
			?>

			<a href="#" class="slide-arrow slide-arrow--next">
				<i class="fal fa-chevron-right"></i>
			</a>

		</div>

	</div>

</div>